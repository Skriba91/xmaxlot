import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Path fileName = Paths.get("nevek.txt");
		Lot lot;
		if(Files.exists(fileName)) {
			try {
				List<String> names = new ArrayList<>();
				names = Files.readAllLines(fileName);
				lot = new Lot(names);
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
		else {
			int participants;
			try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
				System.out.print("Give the number of participants: ");
				participants = Integer.parseInt(br.readLine());
				System.out.println("Give the name of participants.\nEvery name in a new line!");
				lot = new Lot();
				for(int i = 0; i < participants; ++i)
					lot.newName(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
			
		}
		lot.printPeople();
		while(!lot.MakeItSo());
		lot.saveResult();

	}

}
