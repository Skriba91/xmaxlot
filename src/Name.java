import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 
 * @author DANI
 *
 */
public class Name {
	
	public String fromName;	//The person who gives the presen
	private String toName;		//The person who gets Thread present
	
	Name(String fromName) {
		toName = null;
		this.fromName = fromName;
	}
	
	/**
	 * Sets the person who gets the present.
	 * @param toName The person who gets the present
	 */
	void SetToName(String toName) throws IllegalArgumentException { 
		if(toName.equals(fromName))
			throw new IllegalArgumentException();
		 this.toName = toName;
	 }
	
	void SaveName() {
		if(toName != null) { //If toName is set the file is saveable
			Path fileName = Paths.get(fromName + ".txt");
			try {
				Files.deleteIfExists(fileName);
				Files.createFile(fileName);
				BufferedWriter bw = Files.newBufferedWriter(fileName);
				bw.write(toName);
				bw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	 

}
