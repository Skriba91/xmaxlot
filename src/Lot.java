import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Lot {
	
	List<String> classNames;
	List<Name> people;
	
	Lot(List<String> names) {
		classNames = names;
		people = new ArrayList<>(names.size());
		for(String s : names)
			people.add(new Name(s));
	}
	
	public Lot() {
		classNames = new ArrayList<>();
		people = new ArrayList<>();
	}

	public void newName(String newName) {
		classNames.add(newName);
		people.add(new Name(newName));
	}
	
	boolean MakeItSo() {
		List<String> names = new ArrayList<>(classNames);
		Random r3 = new Random(getSeed());
		int toNameInt;
		for(Name n : people) {
			do {
				toNameInt = r3.nextInt(names.size());
			} while (n.fromName.equals(names.get(toNameInt)) && names.size() != 1);
			try {
				n.SetToName(names.get(toNameInt));
				names.remove(toNameInt);
			}
			catch (IllegalArgumentException e) {
				return false;
			}
			
			
		}
		return true;
	}
	
	void saveResult() {
		for(Name n : people)
			n.SaveName();
	}
	
	void printPeople() {
		for(Name n : people)
			System.out.println(n.fromName);
	}
		
	
	private long getSeed() {
		Point P;
		PointerInfo a;
		a = MouseInfo.getPointerInfo();
		P = a.getLocation();
		Random r1 = new Random(System.currentTimeMillis() + Double.doubleToLongBits(P.getX() + P.getY()));
		long seed; //The final seed for the random number
		Runtime runtime = Runtime.getRuntime();		//Memory data to calculate final seed
		long totalMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();
		long maxMemory = runtime.maxMemory();
		long usedMemory = totalMemory - freeMemory;
		if(r1.nextInt() < r1.nextInt()) {
			a = MouseInfo.getPointerInfo();
			P = a.getLocation();
			double seedValue = 1586.6 - 708.75;			//Temp variable for seed
			Random r2 = new Random(System.currentTimeMillis()*r1.nextLong() + Double.doubleToLongBits(P.getX()));
			seedValue *= Math.sin((totalMemory*usedMemory)/r2.nextDouble());
			seedValue *= Math.cos((maxMemory/freeMemory)*r2.nextDouble());
			seedValue = Math.abs(seedValue) + (P.getX() + P.getY()) + 1;
			seed = Double.doubleToLongBits(seedValue);
		}
		else {
			a = MouseInfo.getPointerInfo();
			P = a.getLocation();
			double seedValue = 1586.6 - 708.75;			//Temp variable for seed
			Random r2 = new Random(System.currentTimeMillis()*r1.nextLong() + Double.doubleToLongBits(P.getY()));
			seedValue *= Math.cos((totalMemory/usedMemory)/r2.nextDouble());
			seedValue *= Math.sin((maxMemory*freeMemory)*r2.nextDouble());
			seedValue = Math.abs(seedValue) + (P.getX() + P.getY()) + 1;
			seed = Double.doubleToLongBits(seedValue);
			}
		
		return seed;
		}
	
	}
